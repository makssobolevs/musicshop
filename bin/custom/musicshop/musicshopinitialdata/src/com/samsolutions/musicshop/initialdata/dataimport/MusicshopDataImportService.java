package com.samsolutions.musicshop.initialdata.dataimport;

import de.hybris.platform.commerceservices.dataimport.impl.SampleDataImportService;

public class MusicshopDataImportService extends SampleDataImportService {

    @Override
    protected void importProductCatalog(String extensionName, String productCatalogName) {
        super.importProductCatalog(extensionName, productCatalogName);

        getSetupImpexService().importImpexFile(
                String.format("/%s/import/sampledata/productCatalogs/%sProductCatalog/pricegroups.impex", extensionName,
                productCatalogName), false);
    }

    @Override
    protected void importStore(String extensionName, String storeName, String productCatalogName) {
        super.importStore(extensionName, storeName, productCatalogName);

        getSetupImpexService().importImpexFile(
                String.format("/%s/import/sampledata/stores/%s/customers.impex",
                        extensionName, storeName), false);

    }
}
