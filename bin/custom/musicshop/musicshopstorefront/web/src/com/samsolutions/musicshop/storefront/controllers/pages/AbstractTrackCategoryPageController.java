package com.samsolutions.musicshop.storefront.controllers.pages;


import com.samsolutions.musicshop.facades.data.TrackData;
import com.samsolutions.musicshop.storefront.breadcrumb.SearchTrackBreadcrumbBuilder;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorservices.data.RequestContextData;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractCategoryPageController;
import de.hybris.platform.acceleratorstorefrontcommons.util.MetaSanitizerUtil;
import de.hybris.platform.acceleratorstorefrontcommons.util.XSSFilterUtil;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.pages.CategoryPageModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.search.ProductSearchFacade;
import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.facetdata.ProductCategorySearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.ui.Model;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.util.Collections;

@SuppressWarnings("Duplicates")
public abstract class AbstractTrackCategoryPageController extends AbstractCategoryPageController {

    private ProductSearchFacade<TrackData> trackSearchFacade;

    @Resource(name = "searchTrackBreadcrumbBuilder")
    private SearchTrackBreadcrumbBuilder searchTrackBreadcrumbBuilder;


    public ProductSearchFacade<TrackData> getTrackSearchFacade() {
        return trackSearchFacade;
    }

    @Resource(name = "trackSearchFacade")
    public void setTrackSearchFacade(ProductSearchFacade<TrackData> trackSearchFacade) {
        this.trackSearchFacade = trackSearchFacade;
    }

    protected String performSearchAndGetResultsPage(final String categoryCode, final String searchQuery, final int page, // NOSONAR
                                                    final ShowMode showMode, final String sortCode, final Model model, final HttpServletRequest request,
                                                    final HttpServletResponse response) throws UnsupportedEncodingException
    {
        final CategoryModel category = getCommerceCategoryService().getCategoryForCode(categoryCode);

        final String redirection = checkRequestUrl(request, response, getCategoryModelUrlResolver().resolve(category));
        if (StringUtils.isNotEmpty(redirection))
        {
            return redirection;
        }

        final CategoryPageModel categoryPage = getCategoryPage(category);

        final CategorySearchEvaluator categorySearch = new CategorySearchEvaluator(categoryCode, XSSFilterUtil.filter(searchQuery),
                page, showMode, sortCode, categoryPage);

        ProductCategorySearchPageData<SearchStateData, TrackData, CategoryData> searchPageData = null;
        try
        {
            categorySearch.doTrackSearch();
            searchPageData = categorySearch.getTrackSearchPageData();
        }
        catch (final ConversionException e) // NOSONAR
        {
            searchPageData = createEmptyTrackSearchResult(categoryCode);
        }

        final boolean showCategoriesOnly = categorySearch.isShowCategoriesOnly();

        storeCmsPageInModel(model, categorySearch.getCategoryPage());
        storeContinueUrl(request);

        populateModel(model, searchPageData, showMode);
        model.addAttribute(WebConstants.BREADCRUMBS_KEY, getSearchTrackBreadcrumbBuilder().getTrackBreadcrumbs(categoryCode, searchPageData));
        model.addAttribute("showCategoriesOnly", Boolean.valueOf(showCategoriesOnly));
        model.addAttribute("categoryName", category.getName());
        model.addAttribute("pageType", PageType.CATEGORY.name());
        model.addAttribute("userLocation", getCustomerLocationService().getUserLocation());

        updatePageTitle(category, searchPageData.getBreadcrumbs(), model);

        final RequestContextData requestContextData = getRequestContextData(request);
        requestContextData.setCategory(category);
        requestContextData.setSearch(searchPageData);

        if (searchQuery != null)
        {
            model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_FOLLOW);
        }

        final String metaKeywords = MetaSanitizerUtil.sanitizeKeywords(category.getKeywords());
        final String metaDescription = MetaSanitizerUtil.sanitizeDescription(category.getDescription());
        setUpMetaData(model, metaKeywords, metaDescription);

        return getViewPage(categorySearch.getCategoryPage());

    }


    protected ProductCategorySearchPageData<SearchStateData, TrackData, CategoryData> createEmptyTrackSearchResult(final String categoryCode)
    {
    ProductCategorySearchPageData<SearchStateData, TrackData, CategoryData> searchPageData = new ProductCategorySearchPageData<>();
        searchPageData.setResults(Collections.<TrackData> emptyList());
        searchPageData.setPagination(createEmptyPagination());
        searchPageData.setCategoryCode(categoryCode);
        return searchPageData;
    }


    protected class CategorySearchEvaluator extends AbstractCategoryPageController.CategorySearchEvaluator {


        private final String categoryCode;
        private final SearchQueryData searchQueryData = new SearchQueryData();
        private final int page;
        private final ShowMode showMode;
        private final String sortCode;
        private CategoryPageModel categoryPage;
        private boolean showCategoriesOnly;
        private ProductCategorySearchPageData<SearchStateData, TrackData, CategoryData> trackSearchPageData;

        public CategorySearchEvaluator(final String categoryCode, final String searchQuery, final int page,
                                       final ShowMode showMode, final String sortCode, final CategoryPageModel categoryPage)
        {
            super(categoryCode, searchQuery, page, showMode, sortCode ,categoryPage);
            this.categoryCode = categoryCode;
            this.searchQueryData.setValue(searchQuery);
            this.page = page;
            this.showMode = showMode;
            this.sortCode = sortCode;
            this.categoryPage = categoryPage;
        }

        public void doTrackSearch() {
            showCategoriesOnly = false;
            if (searchQueryData.getValue() == null) {
                // Direct category link without filtering
                trackSearchPageData = getTrackSearchFacade().categorySearch(categoryCode);
                if (categoryPage != null) {
                    showCategoriesOnly = !categoryHasDefaultPage(categoryPage)
                            && CollectionUtils.isNotEmpty(trackSearchPageData.getSubCategories());
                }
            } else {
                // We have some search filtering
                if (categoryPage == null || !categoryHasDefaultPage(categoryPage)) {
                    // Load the default category page
                    categoryPage = getDefaultCategoryPage();
                }

                final SearchStateData searchState = new SearchStateData();
                searchState.setQuery(searchQueryData);

                final PageableData pageableData = createPageableData(page, getSearchPageSize(), sortCode, showMode);
                trackSearchPageData = getTrackSearchFacade().categorySearch(categoryCode, searchState, pageableData);
            }
        }

        public ProductCategorySearchPageData<SearchStateData, TrackData, CategoryData> getTrackSearchPageData() {
            return trackSearchPageData;
        }



    }

    public SearchTrackBreadcrumbBuilder getSearchTrackBreadcrumbBuilder()
    {
        return searchTrackBreadcrumbBuilder;
    }
}
