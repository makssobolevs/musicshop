package com.samsolutions.musicshop.storefront.controllers.cms;


import com.samsolutions.musicshop.core.model.MusicshopCarouselComponentModel;
import com.samsolutions.musicshop.facades.MusicshopProductFacade;
import com.samsolutions.musicshop.storefront.controllers.ControllerConstants;
import de.hybris.platform.acceleratorcms.model.actions.ListAddToCartActionModel;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.samsolutions.musicshop.facades.constants.MusicshopFacadesConstants.PRODUCT_LATEST_COOKIE_DIVIDER;
import static com.samsolutions.musicshop.facades.constants.MusicshopFacadesConstants.PRODUCT_LATEST_COOKIE_HEADER;
import static com.samsolutions.musicshop.facades.constants.MusicshopFacadesConstants.PRODUCT_LATEST_COOKIE_MIN_NUMBER;

@Controller("MusicshopCarouselComponentController")
@Scope("tenant")
@RequestMapping(value = ControllerConstants.Actions.Cms.MusicshopCarouselComponent)
public class MusicshopCarouselComponentController extends
        AbstractAcceleratorCMSComponentController<MusicshopCarouselComponentModel> {

    private static final Logger log = LoggerFactory.getLogger(MusicshopCarouselComponentController.class);

    private MusicshopProductFacade productFacade;

    @Resource(name = "productFacade")
    public void setProductFacade(MusicshopProductFacade productFacade) {
        this.productFacade = productFacade;
    }

    @Override
    protected void fillModel(final HttpServletRequest request, Model model, MusicshopCarouselComponentModel component) {

        Optional<Cookie> latestCookie = Arrays.stream(request.getCookies()).filter(
                c -> c.getName().equals(PRODUCT_LATEST_COOKIE_HEADER)
        ).findFirst();

        latestCookie.ifPresent(cookie -> {

            String[] codes = cookie.getValue().split(PRODUCT_LATEST_COOKIE_DIVIDER);

            if (codes.length > PRODUCT_LATEST_COOKIE_MIN_NUMBER) {
                proceedWithCodes(codes, model);
            }
        });



    }

    private void proceedWithCodes(String[] codes, Model model) {

        try {
            final List<ProductOption> options = new ArrayList<>(Arrays.asList(ProductOption.VARIANT_MATRIX_BASE, ProductOption.VARIANT_MATRIX_URL,
                     ProductOption.BASIC,
                    ProductOption.URL, ProductOption.PRICE, ProductOption.SUMMARY, ProductOption.IMAGES
                    ));

            List<ProductData> products = Arrays.stream(codes).map(
                    code -> productFacade.getProductForCodeAndOptions(code, options)
            ).collect(Collectors.toList());

            model.addAttribute("latestProducts", products);

        } catch (UnknownIdentifierException e) {
            log.error("Unknow code in latest products cookies");
            e.printStackTrace();
        }


    }

}
