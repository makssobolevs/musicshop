package com.samsolutions.musicshop.storefront.checkout.steps.validation.impl;

import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.commercefacades.order.data.CartData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

public class MusicshopSummaryCheckoutStepValidator extends DefaultSummaryCheckoutStepValidator {

    private static final Logger LOGGER = LoggerFactory.getLogger(MusicshopPaymentCheckoutStepValidator.class);

    @Override
    protected ValidationResults checkCartAndDelivery(final RedirectAttributes redirectAttributes) {

        if (!getCheckoutFlowFacade().hasValidCart())
        {
            LOGGER.info("Missing, empty or unsupported cart");
            return ValidationResults.REDIRECT_TO_CART;
        }

        return null;
    }

    @Override
    protected ValidationResults checkPaymentMethodAndPickup(RedirectAttributes redirectAttributes) {
        if (getCheckoutFlowFacade().hasNoPaymentInfo())
        {
            GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
                    "checkout.multi.paymentDetails.notprovided");
            return ValidationResults.REDIRECT_TO_PAYMENT_METHOD;
        }

        final CartData cartData = getCheckoutFacade().getCheckoutCart();

        if (!getCheckoutFacade().hasShippingItems())
        {
            cartData.setDeliveryAddress(null);
        }

        return null;
    }
}
