package com.samsolutions.musicshop.storefront.controllers.pages;

import com.samsolutions.musicshop.core.model.TrackModel;
import com.samsolutions.musicshop.core.services.TrackService;
import com.samsolutions.musicshop.facades.data.TrackData;
import com.samsolutions.musicshop.facades.track.TrackFacade;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.forms.ReviewForm;
import de.hybris.platform.acceleratorstorefrontcommons.util.MetaSanitizerUtil;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.product.ProductOption;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.samsolutions.musicshop.facades.constants.MusicshopFacadesConstants.PRODUCT_LATEST_COOKIE_HEADER;

@Controller
@Scope("tenant")
@RequestMapping(value = "/**/track")
@SuppressWarnings("unused")
public class TrackPageController extends ProductPageController{

    private static final Logger log = LoggerFactory.getLogger(TrackPageController.class);

    private TrackFacade productFacade;

    private TrackService trackService;

    @Resource(name = "trackFacade")
    public void setProductFacade(TrackFacade productFacade) {
        this.productFacade = productFacade;
    }


    @Resource(name = "trackService")
    public void setTrackService(TrackService trackService) {
        this.trackService = trackService;
    }

    @RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
    public String productDetail(@PathVariable("productCode") final String productCode, final Model model,
                                final HttpServletRequest request,
                                @CookieValue(value = PRODUCT_LATEST_COOKIE_HEADER, defaultValue = StringUtils.EMPTY) String reviewedCookie,
                                final HttpServletResponse response)
            throws CMSItemNotFoundException, UnsupportedEncodingException
    {

        TrackData productData = populateAndGetProductDetailForDisplay(productCode, model, request);

        final String redirection = checkRequestUrl(request, response, getProductDataUrlResolver().resolve(productData));
        if (StringUtils.isNotEmpty(redirection))
        {
            return redirection;
        }

        updatePageTitle(productCode, model);

        processCookie(response, reviewedCookie, productCode);


        model.addAttribute(new ReviewForm());
        model.addAttribute("pageType", PageType.PRODUCT.name());
        model.addAttribute("futureStockEnabled", false);

        final String metaKeywords = MetaSanitizerUtil.sanitizeKeywords(productData.getKeywords());
        final String metaDescription = MetaSanitizerUtil.sanitizeDescription(productData.getDescription());
        setUpMetaData(model, metaKeywords, metaDescription);
        return getViewForPage(model);
    }

    private TrackData populateAndGetProductDetailForDisplay(String productCode, Model model,
                                                                      HttpServletRequest request)
            throws CMSItemNotFoundException {

        TrackModel trackModel = trackService.getTrackForCode(productCode).get();

        getRequestContextData(request).setProduct(trackModel);

        final List<ProductOption> options = new ArrayList<>(Arrays.asList(ProductOption.VARIANT_MATRIX_BASE, ProductOption.VARIANT_MATRIX_URL,
				ProductOption.VARIANT_MATRIX_MEDIA, ProductOption.BASIC,
                ProductOption.URL, ProductOption.PRICE, ProductOption.SUMMARY, ProductOption.DESCRIPTION,
                ProductOption.IMAGES, ProductOption.GALLERY,
                ProductOption.CATEGORIES, ProductOption.REVIEW, ProductOption.PROMOTIONS, ProductOption.CLASSIFICATION,
                ProductOption.VARIANT_FULL
                ));

        final TrackData productData = productFacade.getTrackForCodeAndOptions(productCode, options).get();
        sortVariantOptionData(productData);
        storeCmsPageInModel(model, getPageForProduct(productCode));
        populateProductData(productData, model);

        model.addAttribute(WebConstants.BREADCRUMBS_KEY, productBreadcrumbBuilder.getBreadcrumbs(productCode));

        if (CollectionUtils.isNotEmpty(productData.getVariantMatrix()))
        {
            model.addAttribute(WebConstants.MULTI_DIMENSIONAL_PRODUCT,
                    Boolean.valueOf(CollectionUtils.isNotEmpty(productData.getVariantMatrix())));
        }
        return productData;
    }


}
