package com.samsolutions.musicshop.storefront.breadcrumb;

import com.samsolutions.musicshop.facades.data.AlbumData;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.SearchBreadcrumbBuilder;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData;
import org.apache.commons.collections.CollectionUtils;

import java.util.List;

public class SearchAlbumBreadcrumbBuilder extends SearchBreadcrumbBuilder {

    public List<Breadcrumb> getAlbumBreadcrumbs(final String categoryCode,
                                                final ProductSearchPageData<SearchStateData, AlbumData> searchPageData)
    {
        final boolean emptyBreadcrumbs = CollectionUtils.isEmpty(searchPageData.getBreadcrumbs());
        final String searchText = searchPageData.getFreeTextSearch();

        return getBreadcrumbs(categoryCode, searchText, emptyBreadcrumbs);
    }
}
