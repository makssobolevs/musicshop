package com.samsolutions.musicshop.storefront.checkout.steps.validation.impl;

import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.AbstractCheckoutStepValidator;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

public class MusicshopPaymentCheckoutStepValidator extends AbstractCheckoutStepValidator {

    private static final Logger log = LoggerFactory.getLogger(MusicshopPaymentCheckoutStepValidator.class);

    @Override
    public ValidationResults validateOnEnter(RedirectAttributes redirectAttributes) {
        if (!getCheckoutFlowFacade().hasValidCart())
        {
            log.info("Missing, empty or unsupported cart");
            return ValidationResults.REDIRECT_TO_CART;
        }
        return ValidationResults.SUCCESS;
    }
}
