package com.samsolutions.musicshop.storefront.breadcrumb;

import com.samsolutions.musicshop.facades.data.TrackData;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.SearchBreadcrumbBuilder;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData;
import org.apache.commons.collections.CollectionUtils;

import java.util.List;

public class SearchTrackBreadcrumbBuilder extends SearchBreadcrumbBuilder {

    public List<Breadcrumb> getTrackBreadcrumbs(final String categoryCode,
                                                final ProductSearchPageData<SearchStateData, TrackData> searchPageData)
    {
        final boolean emptyBreadcrumbs = CollectionUtils.isEmpty(searchPageData.getBreadcrumbs());
        final String searchText = searchPageData.getFreeTextSearch();

        return getBreadcrumbs(categoryCode, searchText, emptyBreadcrumbs);
    }
}
