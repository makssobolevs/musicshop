<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page trimDirectiveWhitespaces="true" %>


<div class="tabHead">${component.title}</div>
<div class="tabBody">
    <iframe class="video" width="420" height="315" src="${product.videoUrl}" >
    </iframe>
</div>
