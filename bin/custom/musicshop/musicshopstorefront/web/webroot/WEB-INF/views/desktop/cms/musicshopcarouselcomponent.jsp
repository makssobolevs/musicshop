<%@ page trimDirectiveWhitespaces="true" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="action" tagdir="/WEB-INF/tags/desktop/action" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<c:if test="${not empty latestProducts}" >
  <div class="carousel">
    <c:forEach items="${latestProducts}" var="product">

      <c:url var="productUrl" value="${product.url}" />

      <div class="carouselContainer" >
        <div class="carouselInner">
          <div class="head">
              ${product.name}
          </div>
          <div class="thumb">
            <a href="${productUrl}"><product:productPrimaryImage product="${product}" format="thumbnail"/></a>
          </div>
          <ycommerce:testId code="searchPage_price_label_${product.code}">
            <div class="priceContainer"><format:fromPrice priceData="${product.price}"/></div>
          </ycommerce:testId>

          <c:url value="/cart/add" var="addToCartUrl"/>
          <spring:theme code="text.addToCart" var="addToCartText"/>
          <c:set var="product" value="${product}" scope="request"/>
          <c:set var="addToCartText" value="${addToCartText}" scope="request"/>
          <c:set var="addToCartUrl" value="${addToCartUrl}" scope="request"/>
          <c:set var="component" value="${component}" scope="request"/>
          <c:set value="${component.actions}" var="actions" scope="request" />

          <div id="actions-container-for-${component.uid}" class="musicshopCarouselComponentContainer clearfix">
            <action:actions element="div" parentComponent="${component}"/>
          </div>
        </div>
      </div>
    </c:forEach>
  </div>
</c:if>

