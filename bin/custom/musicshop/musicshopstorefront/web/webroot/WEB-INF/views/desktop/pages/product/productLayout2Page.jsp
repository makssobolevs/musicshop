<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="pageScripts" value="/WEB-INF/views/desktop/fragments/scripts/productDetailsScripts.jsp" />


<template:page pageTitle="${pageTitle}">
	<jsp:attribute name="pageScripts">
        <script type="text/javascript" src="${commonResourcePath}/js/likes.js"></script>
	</jsp:attribute>

    <jsp:body>

        <div id="globalMessages">
            <common:globalMessages/>
        </div>
        <cms:pageSlot position="Section1" var="comp" element="div" class="span-24 section1 cms_disp-img_slot">
            <cms:component component="${comp}"/>
        </cms:pageSlot>
        <product:productDetailsPanel product="${product}" galleryImages="${galleryImages}"/>
        <cms:pageSlot position="CrossSelling" var="comp" element="div" class="span-24">
            <cms:component component="${comp}"/>
        </cms:pageSlot>
        <cms:pageSlot position="Section3" var="feature" element="div" class="span-20 section3 cms_disp-img_slot">
            <cms:component component="${feature}"/>
        </cms:pageSlot>
        <cms:pageSlot position="UpSelling" var="comp" element="div" class="span-24">
            <cms:component component="${comp}"/>
        </cms:pageSlot>
        <product:productPageTabs />
        <cms:pageSlot position="Section4" var="feature" element="div" class="span-24 section4 cms_disp-img_slot">
            <cms:component component="${feature}"/>
        </cms:pageSlot>

    </jsp:body>

</template:page>