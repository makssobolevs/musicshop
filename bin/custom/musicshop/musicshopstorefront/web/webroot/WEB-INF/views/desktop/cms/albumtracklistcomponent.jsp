<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page trimDirectiveWhitespaces="true" %>

<c:forEach items="${product.tracks}" var="track" >
    <span class="trackName"><c:out value="${track.name}" /></span>

    <audio controls>
        <source src="${track.demoUrl}" type="audio/mpeg"/>
    </audio>

</c:forEach>
