var likesService = (function () {

    var beforeRequest = function(request){
        request.setRequestHeader("CSRFToken", ACC.config.CSRFToken)
    };

    var ajaxTemplate = function (method, msg, async) {
        var ajax = $.ajax({
            method: method,
            url: window.location.href + '/reviewLike' ,
            contentType: "application/json; charset=utf-8",
            async: async,
            data: msg,
            beforeSend: beforeRequest
        });
        return ajax;
    };

    var methods = {
        postLike: function (msg) {
            return ajaxTemplate('POST', JSON.stringify(msg), true);
        }
    };

    return methods;

})();

var reviewLikesContoller = (function () {

    var ACTION_LIKE = "LIKE";
    var ACTION_DISLIKE = "DISLIKE";

    function LikeData(code, action) {
        this.reviewCode = code;
        this.action = action;
    }

    function like(a) {
        var code = a.closest(".reviewDetail").attr('id');
        var likeData = new LikeData(code, ACTION_LIKE);
        likesService.postLike(likeData)
            .done(function() {
                var elementLikes = $('#numberLike');
                var nowLikes = + elementLikes.text();
                var newLikes = nowLikes + 1;
                elementLikes.text(newLikes);
            })
            .fail(function (err) {
                alert(err.responseText)
            })
    }

    function dislike(a) {
        var code = a.closest("div.reviewDetail").attr('id');
        var likeData = new LikeData(code, ACTION_DISLIKE);
        likesService.postLike(likeData)
            .done(function() {
                var elementLikes = $('#numberLike');
                var nowLikes = + elementLikes.text();
                var newLikes = nowLikes - 1;
                elementLikes.text(newLikes);
            })
            .fail(function (err) {
                alert(err.responseText)
            })
    }

    return {
        like: like,
        dislike: dislike
    }

})();

$(document).ready(function() {

    $(document).on('click', "#buttonLike", function () {
        reviewLikesContoller.like($(this))
    });

    $(document).on('click', "#buttonDislike", function () {
        reviewLikesContoller.dislike($(this))
    });

});