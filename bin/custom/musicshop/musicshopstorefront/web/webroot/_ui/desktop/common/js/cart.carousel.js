var CarouselController = (function () {

    return {
        addToCartSuccessHandler: function (cartResult, statusText, xhr, formElement) {
            ACC.product.displayAddToCartPopup(cartResult, statusText, xhr, formElement);
            ACC.product.disableAddToCartButton(formElement);
            location.reload()
        }
    }

})();


$(document).ready(function () {

    $('.carousel').slick({
        dots: true,
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: false
    });

    var addToCartForm = $('.add_to_cart_form');

    setTimeout(function () {
        addToCartForm.unbind('ajaxForm');
        addToCartForm.ajaxForm({success: CarouselController.addToCartSuccessHandler});
    }, 0);


});