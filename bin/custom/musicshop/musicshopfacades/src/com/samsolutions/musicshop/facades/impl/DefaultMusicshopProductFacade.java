package com.samsolutions.musicshop.facades.impl;

import com.samsolutions.musicshop.core.model.MusicshopCustomerReviewModel;
import com.samsolutions.musicshop.core.services.MusicshopCustomerReviewService;
import com.samsolutions.musicshop.facades.MusicshopProductFacade;
import com.samsolutions.musicshop.facades.data.MusicshopReviewData;
import com.samsolutions.musicshop.facades.data.ReviewLikeData;
import com.samsolutions.musicshop.facades.exception.ActionNotSupportedException;
import com.samsolutions.musicshop.facades.exception.LikeExistException;
import com.samsolutions.musicshop.facades.exception.LikeForbiddenException;
import de.hybris.platform.commercefacades.product.data.ReviewData;
import de.hybris.platform.commercefacades.product.impl.DefaultProductVariantFacade;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import org.springframework.http.HttpStatus;
import org.springframework.util.Assert;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.samsolutions.musicshop.facades.constants.MusicshopFacadesConstants.ACTION_DISLIKE;
import static com.samsolutions.musicshop.facades.constants.MusicshopFacadesConstants.ACTION_LIKE;

public class DefaultMusicshopProductFacade extends DefaultProductVariantFacade
        implements MusicshopProductFacade {

    private AbstractPopulatingConverter<MusicshopCustomerReviewModel, MusicshopReviewData> customerReviewConverter;
    private MusicshopCustomerReviewService customerReviewService;

    public void setCustomerReviewService(MusicshopCustomerReviewService customerReviewService) {
        this.customerReviewService = customerReviewService;
    }

    @Override
    public MusicshopCustomerReviewService getCustomerReviewService() {
        return customerReviewService;
    }


    public void setCustomerReviewConverter(AbstractPopulatingConverter<MusicshopCustomerReviewModel,
            MusicshopReviewData> customerReviewConverter) {
        this.customerReviewConverter = customerReviewConverter;
    }

    @Override
    public List<MusicshopReviewData> getMusicshopReviews(final String productCode, final Integer numberOfReviews)
    {
        final ProductModel product = getProductService().getProductForCode(productCode);
        final List<MusicshopCustomerReviewModel> reviews = customerReviewService
                .getMusicshopReviewsForProductAndLanguage(product,
                getCommonI18NService().getCurrentLanguage());

        if (numberOfReviews == null)
        {
            return customerReviewConverter.convertAll(reviews);
        }
        else if (numberOfReviews.intValue() < 0)
        {
            throw new IllegalArgumentException();
        }
        else
        {
            return customerReviewConverter.convertAll(reviews.subList(0, Math.min(numberOfReviews.intValue(), reviews.size())));
        }
    }


    @Override
    public MusicshopReviewData postReview(final String productCode, final ReviewData reviewData)
    {
        Assert.notNull(reviewData, "Parameter reviewData cannot be null.");
        final ProductModel productModel = getProductService().getProductForCode(productCode);
        final UserModel userModel = getUserService().getCurrentUser();
        final MusicshopCustomerReviewModel customerReviewModel = getCustomerReviewService().createMusicshopCustomerReview(reviewData.getRating(),
                reviewData.getHeadline(), reviewData.getComment(), userModel, productModel);
        customerReviewModel.setLanguage(getCommonI18NService().getCurrentLanguage());
        customerReviewModel.setAlias(reviewData.getAlias());
        getModelService().save(customerReviewModel);
        return customerReviewConverter.convert(customerReviewModel);
    }

    @Override
    public void updatePreviewLikeData(ReviewLikeData reviewLikeData) {

        MusicshopCustomerReviewModel reviewModel = customerReviewService
                .getMusicshopReviewForCode(reviewLikeData.getReviewCode());

        final UserModel userModel = getUserService().getCurrentUser();
        final String action = reviewLikeData.getAction();

        if ((reviewModel != null)
                && !getUserService().isAnonymousUser(userModel)) {

            Set<UserModel> likes = reviewModel.getLikes();
            Set<UserModel> dislikes = reviewModel.getDislikes();

            if ( (likes == null || !likes.contains(userModel))
                    && (dislikes == null || !dislikes.contains(userModel)) ) {
                if (action.equals(ACTION_LIKE)) {
                    Set<UserModel> newLikes = likes != null ? new HashSet<>(likes) : new HashSet<>();
                    newLikes.add(userModel);
                    reviewModel.setLikes(newLikes);
                    getModelService().save(reviewModel);
                }

                else if (action.equals(ACTION_DISLIKE)) {
                    Set<UserModel> newDislikes = dislikes != null ? new HashSet<>(dislikes) : new HashSet<>();
                    newDislikes.add(userModel);
                    reviewModel.setDislikes(newDislikes);
                    getModelService().save(reviewModel);
                }
                else {
                    throw new ActionNotSupportedException("Not supported action", HttpStatus.BAD_REQUEST);
                }
            } else {

                throw new LikeExistException("Like or dislike exists", HttpStatus.CONFLICT);
            }


        } else {
            throw new LikeForbiddenException("Like forbidden", HttpStatus.FORBIDDEN);
        }

    }

    @Override
    public List<MusicshopReviewData> getMusicshopReviews(String productCode) throws UnknownIdentifierException, IllegalArgumentException {
        return getMusicshopReviews(productCode, null);
    }
}

