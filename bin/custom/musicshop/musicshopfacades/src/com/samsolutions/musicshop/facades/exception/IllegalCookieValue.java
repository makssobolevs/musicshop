package com.samsolutions.musicshop.facades.exception;

public class IllegalCookieValue extends RuntimeException {

    public IllegalCookieValue(String s) {
        super(s);
    }
}
