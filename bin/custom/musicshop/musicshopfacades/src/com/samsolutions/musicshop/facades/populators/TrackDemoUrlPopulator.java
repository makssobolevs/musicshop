package com.samsolutions.musicshop.facades.populators;

import com.samsolutions.musicshop.core.model.TrackModel;
import com.samsolutions.musicshop.core.services.TrackService;
import com.samsolutions.musicshop.facades.data.TrackData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.springframework.beans.factory.annotation.Autowired;

public class TrackDemoUrlPopulator <SOURCE extends TrackModel, TARGET extends TrackData> implements Populator<SOURCE, TARGET> {

    private TrackService trackService;

    @Autowired
    public void setTrackService(TrackService trackService) {
        this.trackService = trackService;
    }

    @Override
    public void populate(SOURCE trackModel, TARGET trackData) throws ConversionException {
        if (trackModel != null) {
            String demoUrl = trackService.getDemoTrackUrlForTrack(trackModel);
            trackData.setDemoUrl(demoUrl);
        }
    }
}
