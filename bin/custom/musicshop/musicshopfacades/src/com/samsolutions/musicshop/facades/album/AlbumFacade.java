package com.samsolutions.musicshop.facades.album;

import com.samsolutions.musicshop.facades.data.AlbumData;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;

import java.util.Collection;
import java.util.Optional;

public interface AlbumFacade extends ProductFacade {

    Optional<AlbumData> getAlbumForCode(final String code);

    Optional<AlbumData> getAlbumForCodeAndOptions(final String code, final Collection<ProductOption> options);

}
