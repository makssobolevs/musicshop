/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.samsolutions.musicshop.facades.constants;

/**
 * Global class for all MusicshopFacades constants.
 */
@SuppressWarnings("PMD")
public class MusicshopFacadesConstants extends GeneratedMusicshopFacadesConstants
{
	public static final String EXTENSIONNAME = "musicshopfacades";

	public static final String ACTION_LIKE = "LIKE";

	public static final String ACTION_DISLIKE = "DISLIKE";

	public static final String PRODUCT_LATEST_COOKIE_DIVIDER = ",";

	public static final int PRODUCT_LATEST_COOKIE_MAX_NUMBER = 18;

	public static final int PRODUCT_LATEST_COOKIE_MIN_NUMBER = 3;

	public static final String PRODUCT_LATEST_COOKIE_HEADER = "Reviewed";

	public static final int PRODUCT_LATEST_COOKIE_MAX_AGE = 7 * 24 * 3600;

	private MusicshopFacadesConstants()
	{
		//empty
	}
}
