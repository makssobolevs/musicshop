package com.samsolutions.musicshop.facades;

import com.samsolutions.musicshop.facades.data.MusicshopReviewData;
import com.samsolutions.musicshop.facades.data.ReviewLikeData;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.List;

public interface MusicshopProductFacade extends ProductFacade {


    List<MusicshopReviewData> getMusicshopReviews(String productCode, Integer numberOfReviews)
            throws UnknownIdentifierException, IllegalArgumentException;

    List<MusicshopReviewData> getMusicshopReviews(String productCode)
            throws UnknownIdentifierException, IllegalArgumentException;

    void updatePreviewLikeData(final ReviewLikeData reviewLikeData);

}
