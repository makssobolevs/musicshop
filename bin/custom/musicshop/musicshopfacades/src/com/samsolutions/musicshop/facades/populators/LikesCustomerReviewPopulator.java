package com.samsolutions.musicshop.facades.populators;

import com.samsolutions.musicshop.core.model.MusicshopCustomerReviewModel;
import com.samsolutions.musicshop.facades.data.MusicshopReviewData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class LikesCustomerReviewPopulator<SOURCE extends MusicshopCustomerReviewModel,
        TARGET extends MusicshopReviewData> implements Populator<SOURCE, TARGET> {

    @Override
    public void populate(SOURCE customerReviewModel, TARGET reviewData) throws ConversionException {

        if (customerReviewModel != null) {
            reviewData.setLikes(customerReviewModel.getLikes().size());
            reviewData.setDislikes(customerReviewModel.getDislikes().size());
            reviewData.setCode(customerReviewModel.getCode());
        }

    }

}
