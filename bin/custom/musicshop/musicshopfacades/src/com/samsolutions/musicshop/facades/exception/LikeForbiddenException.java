package com.samsolutions.musicshop.facades.exception;

import org.springframework.http.HttpStatus;

public class LikeForbiddenException extends BaseMusicshopException {

    private HttpStatus httpStatus;

    public LikeForbiddenException(String message, HttpStatus httpStatus) {
        super(message, httpStatus);
    }

    @Override
    public String[] getCodes() {
        return new String[] {"text.exception.likeForbidden"};
    }

    @Override
    public Object[] getArguments() {
        return null;
    }

    @Override
    public String getDefaultMessage() {
        return "Not possible to like post for anonymous user";
    }
}
