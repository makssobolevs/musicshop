package com.samsolutions.musicshop.facades.populators;

import com.samsolutions.musicshop.core.model.TrackModel;
import com.samsolutions.musicshop.facades.data.TrackData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.i18n.I18NService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

public class TrackBasePopulator<SOURCE extends TrackModel, TARGET extends TrackData> implements Populator<SOURCE, TARGET> {

    @Autowired
    private I18NService i18NService;

    @Override
    public void populate(SOURCE trackModel, TARGET trackData) throws ConversionException {

        Assert.notNull(trackData, "Cannot be null");
        Assert.notNull(trackModel, "Cannot be null");

        trackData.setDuration(trackModel.getDuration());
        trackData.setName(trackModel.getName(i18NService.getCurrentLocale()));
        trackData.setVideoUrl(trackModel.getVideoUrl());
    }

}
