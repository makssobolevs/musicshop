package com.samsolutions.musicshop.facades.populators;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

public class ProductInCartPopulator implements Populator<ProductModel, ProductData> {

    @Autowired
    private CartService cartService;

    @Override
    public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException {

        Assert.notNull(productModel, "Cannot be null");
        Assert.notNull(productData, "Product data");

        boolean inCart = cartService.getSessionCart().getEntries().stream().anyMatch(abstractOrderEntryModel ->
                abstractOrderEntryModel.getProduct().getCode().equals(productModel.getCode()));

        productData.setInCart(inCart);

    }

}
