package com.samsolutions.musicshop.facades.exception;

import org.springframework.context.MessageSourceResolvable;
import org.springframework.http.HttpStatus;

public abstract class BaseMusicshopException extends RuntimeException implements MessageSourceResolvable {

    private HttpStatus httpStatus;

    @Override
    public Object[] getArguments() {
        return null;
    }

    public BaseMusicshopException(String message, HttpStatus httpStatus) {
        super(message);
        this.httpStatus = httpStatus;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
