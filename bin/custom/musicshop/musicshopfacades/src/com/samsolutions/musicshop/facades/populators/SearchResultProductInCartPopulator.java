package com.samsolutions.musicshop.facades.populators;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.springframework.util.Assert;

import javax.annotation.Resource;

public class SearchResultProductInCartPopulator implements Populator<SearchResultValueData, ProductData> {

    private CartService cartService;

    @Resource(name = "cartService")
    public void setCartService(CartService cartService) {
        this.cartService = cartService;
    }

    @Override
    public void populate(final SearchResultValueData searchResultValueData,
                         final ProductData productData) throws ConversionException {
        Assert.notNull(searchResultValueData, "Cannot be null");
        Assert.notNull(productData, "Cannot be null");

        final String code = (String) searchResultValueData.getValues().get("code");

        if (code != null) {

            boolean inCart = cartService.getSessionCart().getEntries().stream().anyMatch(abstractOrderEntryModel ->
                    abstractOrderEntryModel.getProduct().getCode().equals(productData.getCode()));

            productData.setInCart(inCart);
        }
        else {
            productData.setInCart(false);
        }


    }

}
