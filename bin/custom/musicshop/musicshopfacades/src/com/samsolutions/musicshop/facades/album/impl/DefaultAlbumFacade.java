package com.samsolutions.musicshop.facades.album.impl;

import com.samsolutions.musicshop.core.model.AlbumModel;
import com.samsolutions.musicshop.core.services.AlbumService;
import com.samsolutions.musicshop.facades.album.AlbumFacade;
import com.samsolutions.musicshop.facades.data.AlbumData;
import com.samsolutions.musicshop.facades.impl.DefaultMusicshopProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;

import java.util.Collection;
import java.util.Optional;

public class DefaultAlbumFacade extends DefaultMusicshopProductFacade implements AlbumFacade {

    private AbstractPopulatingConverter<AlbumModel, AlbumData> albumConverter;
    private AlbumService albumService;

    public void setAlbumConverter(AbstractPopulatingConverter<AlbumModel, AlbumData> albumConverter) {
        this.albumConverter = albumConverter;
    }

    public void setAlbumService(AlbumService albumService) {
        this.albumService = albumService;
    }

    @Override
    public Optional<AlbumData> getAlbumForCode(String code) {
        Optional<AlbumModel> albumModel = albumService.getAlbumForCode(code);

        if (albumModel.isPresent()) {
            return Optional.of(albumConverter.convert(albumModel.get()));
        }

        return Optional.empty();
    }

    @Override
    public Optional<AlbumData> getAlbumForCodeAndOptions(String code, Collection<ProductOption> options) {
        Optional<AlbumModel> albumModel = albumService.getAlbumForCode(code);

        if (albumModel.isPresent()) {

            final AlbumData albumData = albumConverter.convert(albumModel.get());

            if (options != null)
            {
                getProductConfiguredPopulator().populate(albumModel.get(), albumData, options);
            }

            return Optional.of(albumData);

        }
        return Optional.empty();
    }

}

