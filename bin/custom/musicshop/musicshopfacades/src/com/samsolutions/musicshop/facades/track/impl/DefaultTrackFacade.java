package com.samsolutions.musicshop.facades.track.impl;

import com.samsolutions.musicshop.core.model.TrackModel;
import com.samsolutions.musicshop.core.services.TrackService;
import com.samsolutions.musicshop.facades.data.TrackData;
import com.samsolutions.musicshop.facades.impl.DefaultMusicshopProductFacade;
import com.samsolutions.musicshop.facades.track.TrackFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;

import java.util.Collection;
import java.util.Optional;

public class DefaultTrackFacade extends DefaultMusicshopProductFacade implements TrackFacade {

    private TrackService trackService;
    private AbstractPopulatingConverter<TrackModel, TrackData> converter;

    public void setTrackService(TrackService trackService) {
        this.trackService = trackService;
    }

    public void setConverter(AbstractPopulatingConverter<TrackModel, TrackData> converter) {
        this.converter = converter;
    }


    @Override
    public Optional<TrackData> getTrackForCode(String code) {

        Optional<TrackModel> track = trackService.getTrackForCode(code);

        if (track.isPresent()) {
            TrackData trackData = converter.convert(track.get());
            return Optional.of(trackData);
        }

        return Optional.empty();

    }

    @Override
    public Optional<TrackData> getTrackForCodeAndOptions(String code, Collection<ProductOption> options) {
        Optional<TrackModel> trackModel = trackService.getTrackForCode(code);

        if (trackModel.isPresent()) {

            final TrackData trackData = converter.convert(trackModel.get());

            if (options != null)
            {
                getProductConfiguredPopulator().populate(trackModel.get(), trackData, options);
            }

            return Optional.of(trackData);

        }
        return Optional.empty();
    }
}
