package com.samsolutions.musicshop.facades.exception;

import org.springframework.http.HttpStatus;

public class ActionNotSupportedException extends BaseMusicshopException  {

    public ActionNotSupportedException(String message, HttpStatus httpStatus) {
        super(message, httpStatus);
    }

    @Override
    public String[] getCodes() {
        return new String[] {"text.exception.likeActionNotSupported"};
    }

    @Override
    public String getDefaultMessage() {
        return "Like action not supported";
    }
}
