package com.samsolutions.musicshop.facades.track;

import com.samsolutions.musicshop.facades.data.TrackData;
import de.hybris.platform.commercefacades.product.ProductOption;

import java.util.Collection;
import java.util.Optional;

public interface TrackFacade {

    Optional<TrackData> getTrackForCode(String code);

    Optional<TrackData> getTrackForCodeAndOptions(final String code, final Collection<ProductOption> options);
}
