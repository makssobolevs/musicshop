package com.samsolutions.musicshop.facades.populators;

import com.samsolutions.musicshop.core.model.AlbumModel;
import com.samsolutions.musicshop.core.model.TrackModel;
import com.samsolutions.musicshop.facades.data.AlbumData;
import com.samsolutions.musicshop.facades.data.TrackData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.List;

public class AlbumTracksPopulator<SOURCE extends AlbumModel, TARGET extends AlbumData> implements Populator<SOURCE, TARGET> {

    private AbstractPopulatingConverter<TrackModel, TrackData> trackConverter;

    public void setTrackConverter(AbstractPopulatingConverter<TrackModel, TrackData> trackConverter) {
        this.trackConverter = trackConverter;
    }

    @Override
    public void populate(SOURCE albumModel, TARGET albumData) throws ConversionException {
        List<TrackData> trackDatas = trackConverter.convertAll(albumModel.getTracks());
        albumData.setTracks(trackDatas);
    }
}
