package com.samsolutions.musicshop.facades.exception;

import org.springframework.http.HttpStatus;

public class LikeExistException extends BaseMusicshopException {

    public LikeExistException(String message, HttpStatus httpStatus) {
        super(message, httpStatus);
    }

    @Override
    public String[] getCodes() {
        return new String[] {"text.exception.likeExist"};
    }

    @Override
    public String getDefaultMessage() {
        return "You have already liked or disliked this post";
    }
}
