package com.samsolutions.musicshop.core.dao.impl;

import com.samsolutions.musicshop.core.dao.AlbumDAO;
import com.samsolutions.musicshop.core.model.AlbumModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository("albumDAO")
@Transactional
public class DefaultAlbumDAO implements AlbumDAO {
    private FlexibleSearchService flexibleSearchService;

    @Autowired
    public void setFlexibleSearchService(FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

    @Override
    public List<AlbumModel> findAlbumsByCode(String code) {
        final String queryString = "SELECT {t:" + AlbumModel.PK + "} "
                + "FROM {" + AlbumModel._TYPECODE +  " AS t}  WHERE {t:" + AlbumModel.CODE + "}=" + code;

        final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString);

        return flexibleSearchService.<AlbumModel>search(query).getResult();
    }


}
