package com.samsolutions.musicshop.core.services;

import com.samsolutions.musicshop.core.model.AlbumModel;

import java.util.Optional;

public interface AlbumService {

    Optional<AlbumModel> getAlbumForCode(final String code);

}

