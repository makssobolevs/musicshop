package com.samsolutions.musicshop.core.model;

import com.samsolutions.musicshop.core.constants.MusicshopCoreConstants;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TrackDurationDynamicAttributeHandler implements DynamicAttributeHandler<String, TrackModel> {

    private static final Logger log = LoggerFactory.getLogger(TrackDurationDynamicAttributeHandler.class);

    @Override
    public String get(TrackModel track) {

        Integer duration = track.getDurationInSeconds();

        int minutes = duration / 60;
        int seconds = duration % 60;

        return minutes + MusicshopCoreConstants.View.MINUTE_SECOND_DIVIDER + String.format("%02d", seconds);
    }


    @Override
    public void set(TrackModel model, String s) {
        throw new UnsupportedOperationException("Cannot change Track duration");
    }
}
