package com.samsolutions.musicshop.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commerceservices.price.CommercePriceService;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.product.PriceService;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class MusicshopProductPriceValueProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider {
    private FieldNameProvider fieldNameProvider;
    private PriceService priceService;
    private CommercePriceService commercePriceService;
    private PriceDataFactory priceDataFactory;

    public void setPriceDataFactory(PriceDataFactory priceDataFactory) {
        this.priceDataFactory = priceDataFactory;
    }

    public void setCommercePriceService(CommercePriceService commercePriceService) {
        this.commercePriceService = commercePriceService;
    }

    public void setFieldNameProvider(FieldNameProvider fieldNameProvider) {
        this.fieldNameProvider = fieldNameProvider;
    }

    public void setPriceService(PriceService priceService) {
        this.priceService = priceService;
    }

    @Override
    public Collection<FieldValue> getFieldValues(IndexConfig indexConfig, IndexedProperty indexedProperty, Object model) throws FieldValueProviderException {
        ArrayList fieldValues = new ArrayList();

        try {
            List e;
            ProductModel product;
            if(!(model instanceof ProductModel)) {
                throw new FieldValueProviderException("Cannot evaluate price of non-product item");
            } else {
                product = (ProductModel)model;
                String fieldName;
                Iterator var14;
                if(indexConfig.getCurrencies().isEmpty()) {
                    List currency = this.priceService.getPriceInformationsForProduct(product);
                    if(currency != null && !currency.isEmpty()) {
                        PriceInformation price = (PriceInformation)currency.get(0);
                        Double sessionCurrency = price.getPriceValue().getValue();
                        e = this.getRangeNameList(indexedProperty, sessionCurrency);
                        Collection prices = this.fieldNameProvider.getFieldNames(indexedProperty, price.getPriceValue().getCurrencyIso());
                        Iterator fieldNames = prices.iterator();

                        while(true) {
                            while(fieldNames.hasNext()) {
                                String value = (String)fieldNames.next();
                                if(e.isEmpty()) {
                                    fieldValues.add(new FieldValue(value, sessionCurrency));
                                } else {
                                    var14 = e.iterator();

                                    while(var14.hasNext()) {
                                        fieldName = (String)var14.next();
                                        fieldValues.add(new FieldValue(value, fieldName == null?sessionCurrency:fieldName));
                                    }
                                }
                            }

                            return fieldValues;
                        }
                    }
                } else {
                    Iterator price1 = indexConfig.getCurrencies().iterator();

                    label170:
                    while(price1.hasNext()) {
                        CurrencyModel currency1 = (CurrencyModel)price1.next();
                        CurrencyModel sessionCurrency1 = this.i18nService.getCurrentCurrency();

                        try {
                            this.i18nService.setCurrentCurrency(currency1);
                            List prices1 = this.priceService.getPriceInformationsForProduct(product);
                            if(prices1 != null && !prices1.isEmpty()) {
                                Double value1 = ((PriceInformation) prices1.get(0)).getPriceValue().getValue();

                                if (CollectionUtils.isEmpty(product.getVariants()))
                                {
                                    PriceInformation info = commercePriceService.getWebPriceForProduct(product);
                                    if (info != null) {
                                        value1 = info.getPriceValue().getValue();
                                    }
                                }

                                e = this.getRangeNameList(indexedProperty, value1, currency1.getIsocode());
                                Collection fieldNames1 = this.fieldNameProvider.getFieldNames(indexedProperty, currency1.getIsocode().toLowerCase());
                                var14 = fieldNames1.iterator();

                                while(true) {
                                    while(true) {
                                        if(!var14.hasNext()) {
                                            continue label170;
                                        }

                                        fieldName = (String)var14.next();
                                        if(e.isEmpty()) {
                                            fieldValues.add(new FieldValue(fieldName, value1));
                                        } else {
                                            Iterator var16 = e.iterator();

                                            while(var16.hasNext()) {
                                                String rangeName = (String)var16.next();
                                                fieldValues.add(new FieldValue(fieldName, rangeName == null?value1:rangeName));
                                            }
                                        }
                                    }
                                }
                            }
                        } finally {
                            this.i18nService.setCurrentCurrency(sessionCurrency1);
                        }
                    }
                }

                return fieldValues;
            }
        } catch (Exception var21) {
            LOG.error(var21);
            throw new FieldValueProviderException("Cannot evaluate " + indexedProperty.getName() + " using " + this.getClass().getName(), var21);
        }
    }

}
