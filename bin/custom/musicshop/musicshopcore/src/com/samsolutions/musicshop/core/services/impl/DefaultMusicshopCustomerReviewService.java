package com.samsolutions.musicshop.core.services.impl;

import com.samsolutions.musicshop.core.dao.MusicshopCustomerReviewDAO;
import com.samsolutions.musicshop.core.model.MusicshopCustomerReviewModel;
import com.samsolutions.musicshop.core.services.MusicshopCustomerReviewService;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.customerreview.impl.DefaultCustomerReviewService;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public class DefaultMusicshopCustomerReviewService extends DefaultCustomerReviewService
        implements MusicshopCustomerReviewService {

    private MusicshopCustomerReviewDAO customerReviewDAO;

    private static final Logger log = LoggerFactory.getLogger(DefaultMusicshopCustomerReviewService.class);


    @Autowired
    public void setCustomerReviewDAO(MusicshopCustomerReviewDAO customerReviewDAO) {
        this.customerReviewDAO = customerReviewDAO;
    }


    @Override
    public List<MusicshopCustomerReviewModel> getMusicshopReviewsForProductAndLanguage(ProductModel product, LanguageModel language) {
        ServicesUtil.validateParameterNotNullStandardMessage("product", product);
        ServicesUtil.validateParameterNotNullStandardMessage("language", language);
        return customerReviewDAO.getMusicshopReviewsForProductAndLanguage(product, language);
    }

    public MusicshopCustomerReviewModel createMusicshopCustomerReview(Double rating, String headline, String comment, UserModel user, ProductModel product) {

        MusicshopCustomerReviewModel customerReviewModel = getModelService().create(MusicshopCustomerReviewModel.class);
        customerReviewModel.setRating(rating);
        customerReviewModel.setHeadline(headline);
        customerReviewModel.setComment(comment);
        customerReviewModel.setUser(user);
        customerReviewModel.setProduct(product);
        customerReviewModel.setCreationtime(new Date(System.currentTimeMillis()));
        customerReviewModel.setCode(UUID.randomUUID().toString());

        getModelService().save(customerReviewModel);

        return customerReviewModel;
    }

    @Override
    public MusicshopCustomerReviewModel getMusicshopReviewForCode(String code) {
        List<MusicshopCustomerReviewModel> reviews = customerReviewDAO.getMusicshopReviewsForCode(code);
        if (reviews.size() == 1) {
            return  reviews.iterator().next();
        }
        else if (reviews.size() > 1) {
            log.error("Not unique review with code : {}", code);
            throw new AmbiguousIdentifierException("Not unique review code");
        }

        return null;
    }
}
