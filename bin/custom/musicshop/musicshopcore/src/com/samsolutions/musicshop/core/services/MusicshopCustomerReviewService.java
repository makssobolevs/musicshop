package com.samsolutions.musicshop.core.services;

import com.samsolutions.musicshop.core.model.MusicshopCustomerReviewModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.customerreview.CustomerReviewService;

import java.util.List;

public interface MusicshopCustomerReviewService extends CustomerReviewService {

    List<MusicshopCustomerReviewModel> getMusicshopReviewsForProductAndLanguage(ProductModel var1, LanguageModel var2);

    MusicshopCustomerReviewModel createMusicshopCustomerReview(Double rating, String headline, String comment, UserModel user, ProductModel product);

    MusicshopCustomerReviewModel getMusicshopReviewForCode(String code);

}
