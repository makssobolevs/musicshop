/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.samsolutions.musicshop.core.constants;

/**
 * Global class for all MusicshopCore constants. You can add global constants for your extension into this class.
 */
public final class MusicshopCoreConstants extends GeneratedMusicshopCoreConstants
{
	public static final String EXTENSIONNAME = "musicshopcore";

	public static final String DEMO_TRACK_MEDIA_PREFIX = "demotrack";

	public static final String PRODUCT_CATALOG = "musicshopProductCatalog";

	public static final String ONLINE_CATALOG_VERSION = "Online";

	public static final String MP3_MIME = "audio/mpeg";

	public static final String TRACK_MEDIA_FOLDER_NAME = "tracks";

	public static final int DEMO_PREVIEW_PERCENT = 20;



	private MusicshopCoreConstants()
	{
		//empty
	}

	public interface View {

		String MINUTE_SECOND_DIVIDER = ".";
	}

	// implement here constants used by this extension
}
