package com.samsolutions.musicshop.core.jalo;

import de.hybris.platform.customerreview.jalo.CustomerReview;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.type.ComposedType;
import org.apache.log4j.Logger;

public class MusicshopCustomerReview extends GeneratedMusicshopCustomerReview
{
	@SuppressWarnings("unused")
	private final static Logger LOG = Logger.getLogger( MusicshopCustomerReview.class.getName() );
	
	@Override
	public CustomerReview createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes) throws JaloBusinessException
	{
		// business code placed here will be executed before the item is created
		// then create the item
		final CustomerReview item = super.createItem( ctx, type, allAttributes );
		// business code placed here will be executed after the item was created
		// and return the item
		return item;
	}
	
}
