package com.samsolutions.musicshop.core.services.impl;

import com.samsolutions.musicshop.core.dao.TrackDAO;
import com.samsolutions.musicshop.core.model.TrackModel;
import com.samsolutions.musicshop.core.services.TrackService;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaFolderModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.NullInputStream;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.samsolutions.musicshop.core.constants.MusicshopCoreConstants.DEMO_PREVIEW_PERCENT;
import static com.samsolutions.musicshop.core.constants.MusicshopCoreConstants.DEMO_TRACK_MEDIA_PREFIX;
import static com.samsolutions.musicshop.core.constants.MusicshopCoreConstants.MP3_MIME;
import static com.samsolutions.musicshop.core.constants.MusicshopCoreConstants.ONLINE_CATALOG_VERSION;
import static com.samsolutions.musicshop.core.constants.MusicshopCoreConstants.PRODUCT_CATALOG;
import static com.samsolutions.musicshop.core.constants.MusicshopCoreConstants.TRACK_MEDIA_FOLDER_NAME;

@Service("trackService")
public class DefaultTrackService implements TrackService {

    private static final Logger log = LoggerFactory.getLogger(DefaultTrackService.class);

    private TrackDAO trackDAO;
    private CatalogVersionService catalogService;
    private MediaService mediaService;
    private ModelService modelService;

    @Autowired
    public void setTrackDAO(TrackDAO trackDAO) {
        this.trackDAO = trackDAO;
    }

    @Autowired
    public void setCatalogService(CatalogVersionService catalogService) {
        this.catalogService = catalogService;
    }

    @Autowired
    public void setMediaService(MediaService mediaService) {
        this.mediaService = mediaService;
    }

    @Autowired
    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }

    public CatalogVersionModel getCatalogVersion() {
        return catalogService.getCatalogVersion(PRODUCT_CATALOG, ONLINE_CATALOG_VERSION);
    }

    public MediaFolderModel getTrackMediaFolder() {
        return mediaService.getFolder(TRACK_MEDIA_FOLDER_NAME);
    }

    @Override
    public Optional<TrackModel> getTrackForCode(final String code) {
        final List<TrackModel> tracks = trackDAO.findTracksByCode(code);

        if (tracks.size() == 1) {
            return Optional.of(tracks.get(0));
        }
        else if (tracks.size() > 1) {
            log.warn("Ambiguous {} with code {}", TrackModel._TYPECODE, code);
            return Optional.of(tracks.get(0));
        }
        else {
            return Optional.empty();
        }
    }

    @Override
    public String getDemoTrackUrlForTrack(final TrackModel trackModel) {
        final String mediaName = DEMO_TRACK_MEDIA_PREFIX + trackModel.getCode();
        final CatalogVersionModel catalogVersion = getCatalogVersion();

        try{
            final MediaModel savedTrackMedia = mediaService.getMedia(catalogVersion, mediaName);
            return savedTrackMedia.getURL();
        } catch (UnknownIdentifierException e) {
            log.debug("No track media found for code:{}", trackModel);
        }

        try {
            final MediaModel mediaTrack = modelService.create(MediaModel.class);
            mediaTrack.setCatalogVersion(catalogVersion);
            mediaTrack.setCode(mediaName);
            mediaTrack.setMime(MP3_MIME);
            mediaTrack.setRealFileName(mediaName);
            modelService.save(mediaTrack);

            MediaModel mediaModel = trackModel.getContent();
            InputStream demoStream = getPartOfTrackMediaStream(mediaModel, DEMO_PREVIEW_PERCENT);
            mediaService.setStreamForMedia(mediaTrack, demoStream, mediaName, MP3_MIME, getTrackMediaFolder());
            demoStream.close();
            return mediaTrack.getURL();
        } catch (Exception e) {
            log.error("Cannot create demo media for track:{}", trackModel);
        }

        return StringUtils.EMPTY;

    }


    public InputStream getPartOfTrackMediaStream(MediaModel mediaModel, int percent) {
        InputStream baseInputStream = mediaService.getStreamFromMedia(mediaModel);
        try {
            byte[] baseData = IOUtils.toByteArray(baseInputStream);
            long baseLength = baseData.length;
            int resultLength = (int) (baseLength * percent / 100.0);
            byte[] buf = Arrays.copyOf(baseData, resultLength);
            InputStream resultInputStream = new ByteArrayInputStream(buf);
            baseInputStream.close();
            return resultInputStream;
        } catch (IOException e) {
            log.error("Exception trying create demo track input stream: {}", mediaModel.getCode());
        }

        return new NullInputStream(0);

    }
}
