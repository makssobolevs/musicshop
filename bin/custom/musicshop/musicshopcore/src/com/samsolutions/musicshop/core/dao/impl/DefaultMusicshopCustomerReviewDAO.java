package com.samsolutions.musicshop.core.dao.impl;

import com.samsolutions.musicshop.core.dao.MusicshopCustomerReviewDAO;
import com.samsolutions.musicshop.core.model.MusicshopCustomerReviewModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.customerreview.dao.impl.DefaultCustomerReviewDao;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;

@Transactional
public class DefaultMusicshopCustomerReviewDAO extends DefaultCustomerReviewDao implements MusicshopCustomerReviewDAO {

    @Override
    public List<MusicshopCustomerReviewModel> getMusicshopReviewsForProductAndLanguage(
            ProductModel product, LanguageModel language) {

        String query = "SELECT {" + Item.PK + "} FROM {" + MusicshopCustomerReviewModel._TYPECODE
                + "} WHERE {" + "product" + "}=?product AND {" + "language" + "}=?language ORDER BY {" + "creationtime" + "} DESC";
        FlexibleSearchQuery fsQuery = new FlexibleSearchQuery(query);
        fsQuery.addQueryParameter("product", product);
        fsQuery.addQueryParameter("language", language);
        fsQuery.setResultClassList(Collections.singletonList(MusicshopCustomerReviewModel.class));
        SearchResult searchResult = this.getFlexibleSearchService().search(fsQuery);
        return searchResult.getResult();

    }

    @Override
    public List<MusicshopCustomerReviewModel> getMusicshopReviewsForCode(String code) {
        String query = "SELECT {" + Item.PK + "} FROM {" + MusicshopCustomerReviewModel._TYPECODE
                + "} WHERE {" + "code" + "}=?code";
        FlexibleSearchQuery fsQuery = new FlexibleSearchQuery(query);
        fsQuery.addQueryParameter("code", code);
        fsQuery.setResultClassList(Collections.singletonList(MusicshopCustomerReviewModel.class));
        SearchResult searchResult = getFlexibleSearchService().search(fsQuery);
        return searchResult.getResult();
    }
}
