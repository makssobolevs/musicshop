package com.samsolutions.musicshop.core.dao;

import com.samsolutions.musicshop.core.model.MusicshopCustomerReviewModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.customerreview.dao.CustomerReviewDao;

import java.util.List;


public interface MusicshopCustomerReviewDAO extends CustomerReviewDao {

    List<MusicshopCustomerReviewModel> getMusicshopReviewsForProductAndLanguage(ProductModel productModel, LanguageModel languageModel);

    List<MusicshopCustomerReviewModel> getMusicshopReviewsForCode(String code);
}
