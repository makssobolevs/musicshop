package com.samsolutions.musicshop.core.services.impl;

import com.samsolutions.musicshop.core.dao.AlbumDAO;
import com.samsolutions.musicshop.core.model.AlbumModel;
import com.samsolutions.musicshop.core.services.AlbumService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service("albumService")
public class DefaultAlbumService implements AlbumService {

    private AlbumDAO albumDAO;

    @Autowired
    public void setAlbumDAO(AlbumDAO albumDAO) {
        this.albumDAO = albumDAO;
    }

    private static final Logger log = LoggerFactory.getLogger(DefaultAlbumService.class);

    @Override
    public Optional<AlbumModel> getAlbumForCode(String code) {
        final List<AlbumModel> albums = albumDAO.findAlbumsByCode(code);

        if (albums.size() == 1) {
            return Optional.of(albums.get(0));
        }
        else if (albums.size() > 1) {
            log.error("Ambiguous {} with code {}", AlbumModel._TYPECODE, code);
            return Optional.of(albums.get(0));
        }
        else {
            return Optional.empty();
        }
    }

}
