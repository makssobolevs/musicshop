package com.samsolutions.musicshop.core.model;

import com.samsolutions.musicshop.core.constants.MusicshopCoreConstants;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

public class AlbumDurationDynamicAttributeHandler implements DynamicAttributeHandler<String, AlbumModel> {

    @Override
    public String get(AlbumModel album) {

        int duration = album.getTracks().stream().mapToInt(TrackModel::getDurationInSeconds).sum();

        int minutes = duration / 60;
        int seconds = duration % 60;

        return minutes + MusicshopCoreConstants.View.MINUTE_SECOND_DIVIDER + String.format("%02d", seconds);
    }

    @Override
    public void set(AlbumModel album, String s) {

        throw new UnsupportedOperationException("Cannot change Track duration");

    }
}
