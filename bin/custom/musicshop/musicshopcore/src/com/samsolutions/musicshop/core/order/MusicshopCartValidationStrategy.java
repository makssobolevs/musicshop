package com.samsolutions.musicshop.core.order;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.commerceservices.strategies.impl.DefaultCartValidationStrategy;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import org.junit.Assert;


public class MusicshopCartValidationStrategy extends DefaultCartValidationStrategy {

    @SuppressWarnings("Duplicates")
    @Override
    protected CommerceCartModification validateCartEntry(CartModel cartModel, CartEntryModel cartEntryModel) {

        try
        {
            getProductService().getProductForCode(cartEntryModel.getProduct().getCode());
        }
        catch (final UnknownIdentifierException e)
        {
            final CommerceCartModification modification = new CommerceCartModification();
            modification.setStatusCode(CommerceCartModificationStatus.UNAVAILABLE);
            modification.setQuantityAdded(0);
            modification.setQuantity(0);

            final CartEntryModel entry = new CartEntryModel();
            entry.setProduct(cartEntryModel.getProduct());

            modification.setEntry(entry);

            getModelService().remove(cartEntryModel);
            getModelService().refresh(cartModel);

            return modification;
        }


        // Stock quantity for this cartEntry
        final long cartEntryLevel = cartEntryModel.getQuantity();
        Assert.assertEquals(cartEntryLevel, 1);


        final CommerceCartModification modification = new CommerceCartModification();
        modification.setStatusCode(CommerceCartModificationStatus.SUCCESS);
        modification.setQuantityAdded(cartEntryLevel);
        modification.setQuantity(cartEntryLevel);
        modification.setEntry(cartEntryModel);

        return modification;
    }

}
