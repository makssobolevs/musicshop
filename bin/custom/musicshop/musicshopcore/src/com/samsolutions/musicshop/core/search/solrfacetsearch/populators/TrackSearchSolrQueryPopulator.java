package com.samsolutions.musicshop.core.search.solrfacetsearch.populators;

import de.hybris.platform.commerceservices.search.solrfacetsearch.populators.SearchSolrQueryPopulator;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import org.apache.log4j.Logger;

import java.util.Collection;
import java.util.Optional;

public class TrackSearchSolrQueryPopulator<INDEXED_PROPERTY_TYPE, INDEXED_TYPE_SORT_TYPE>
        extends SearchSolrQueryPopulator<INDEXED_PROPERTY_TYPE, INDEXED_TYPE_SORT_TYPE> {

    private static final Logger LOG = Logger.getLogger(TrackSearchSolrQueryPopulator.class);

    private static final String INDEXED_TYPE_CODE = "Track";


    protected IndexedType getIndexedType(final FacetSearchConfig config)
    {
        final IndexConfig indexConfig = config.getIndexConfig();

        // Strategy for working out which of the available indexed types to use
        final Collection<IndexedType> indexedTypes = indexConfig.getIndexedTypes().values();
        Optional<IndexedType> indexedType = indexedTypes.stream().filter(it -> it.getCode()
                .equals(INDEXED_TYPE_CODE)).findFirst();

        return indexedType.orElse(null);
    }


}
