package com.samsolutions.musicshop.core.order;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceAddToCartStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

@SuppressWarnings("Duplicates")
public class MusicshopAddToCartStrategy extends DefaultCommerceAddToCartStrategy {

    private static final long ALLOWED_QUANTITY = 1;

    @Override
    public CommerceCartModification addToCart(CommerceCartParameter parameter)
            throws CommerceCartModificationException {

        CommerceCartModification modification;

        final CartModel cartModel = parameter.getCart();
        final ProductModel productModel = parameter.getProduct();
        final PointOfServiceModel deliveryPointOfService = parameter.getPointOfService();

        this.beforeAddToCart(parameter);
        validateAddToCart(parameter);

        if (isProductForCode(parameter)) {

            final long cartLevel = checkCartLevel(productModel, cartModel, deliveryPointOfService);
            if (cartLevel > 0) {
                String statusCode = CommerceCartModificationStatus.MAX_ORDER_QUANTITY_EXCEEDED;
                modification = createAddToCartResp(parameter, statusCode, createEmptyCartEntry(parameter), 0);
            } else {
                final long cartLevelAfterQuantityChange = ALLOWED_QUANTITY;
                final CartEntryModel entryModel = addCartEntry(parameter, cartLevelAfterQuantityChange);
                String statusCode = CommerceCartModificationStatus.SUCCESS;
                modification = createAddToCartResp(parameter, statusCode, entryModel, cartLevelAfterQuantityChange);
            }
        }
        else
        {
            modification = createAddToCartResp(parameter, CommerceCartModificationStatus.UNAVAILABLE,
                    createEmptyCartEntry(parameter), 0);
        }


        afterAddToCart(parameter, modification);

        return modification;
    }


    private Boolean isProductForCode(final CommerceCartParameter parameter)
    {

        final ProductModel productModel = parameter.getProduct();
        try
        {
            getProductService().getProductForCode(productModel.getCode());
        }
        catch (final UnknownIdentifierException e)
        {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }


    private CommerceCartModification createAddToCartResp(final CommerceCartParameter parameter, final String status,
                                                         final CartEntryModel entry, final long quantityAdded)
    {
        final long quantityToAdd = parameter.getQuantity();

        final CommerceCartModification modification = new CommerceCartModification();
        modification.setStatusCode(status);
        modification.setQuantityAdded(quantityAdded);
        modification.setQuantity(quantityToAdd);

        modification.setEntry(entry);

        return modification;
    }


    private CartEntryModel createEmptyCartEntry(final CommerceCartParameter parameter)
    {

        final ProductModel productModel = parameter.getProduct();
        final PointOfServiceModel deliveryPointOfService = parameter.getPointOfService();

        final CartEntryModel entry = new CartEntryModel();
        entry.setProduct(productModel);
        entry.setDeliveryPointOfService(deliveryPointOfService);

        return entry;
    }

    private CartEntryModel addCartEntry(final CommerceCartParameter parameter, final long actualAllowedQuantityChange)
            throws CommerceCartModificationException
    {

        final CartModel cartModel = parameter.getCart();
        final ProductModel productModel = parameter.getProduct();
        final PointOfServiceModel deliveryPointOfService = parameter.getPointOfService();
        final UnitModel unit = parameter.getUnit();

        final boolean forceNewEntry = parameter.isCreateNewEntry();

        final UnitModel orderableUnit = (unit != null ? unit : getUnit(parameter));

        // We are allowed to add items to the cart
        CartEntryModel cartEntryModel;

        if (deliveryPointOfService == null)
        {
            // Modify the cart
            cartEntryModel = getCartService().addNewEntry(cartModel, productModel, actualAllowedQuantityChange, orderableUnit,
                    APPEND_AS_LAST, !forceNewEntry);
        }
        else
        {
            // Find the entry to modify
            final Integer entryNumber = getEntryForProductAndPointOfService(cartModel, productModel, deliveryPointOfService);

            // Modify the cart
            cartEntryModel = getCartService().addNewEntry(cartModel, productModel, actualAllowedQuantityChange, orderableUnit,
                    entryNumber.intValue(), (entryNumber.intValue() < 0) ? false : !forceNewEntry);

            if (cartEntryModel != null)
            {
                cartEntryModel.setDeliveryPointOfService(deliveryPointOfService);
            }
        }

        getModelService().save(cartEntryModel);
        getCommerceCartCalculationStrategy().calculateCart(cartModel);
        getModelService().save(cartEntryModel);

        return cartEntryModel;
    }

    private UnitModel getUnit(final CommerceCartParameter parameter) throws CommerceCartModificationException
    {
        final ProductModel productModel = parameter.getProduct();
        try
        {
            return getProductService().getOrderableUnit(productModel);
        }
        catch (final ModelNotFoundException e)
        {
            throw new CommerceCartModificationException(e.getMessage(), e);
        }
    }
}
