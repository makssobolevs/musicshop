package com.samsolutions.musicshop.core.dao;

import com.samsolutions.musicshop.core.model.AlbumModel;

import java.util.List;

public interface AlbumDAO {

    List<AlbumModel> findAlbumsByCode(String code);
}
