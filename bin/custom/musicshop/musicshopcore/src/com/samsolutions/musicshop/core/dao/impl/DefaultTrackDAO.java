package com.samsolutions.musicshop.core.dao.impl;

import com.samsolutions.musicshop.core.dao.TrackDAO;
import com.samsolutions.musicshop.core.model.TrackModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository("trackDAO")
@Transactional
public class DefaultTrackDAO implements TrackDAO {

    private FlexibleSearchService flexibleSearchService;

    @Autowired
    public void setFlexibleSearchService(FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

    @Override
    @Transactional(readOnly = true)
    public List<TrackModel> findTracksByCode(String code) {
        final String queryString = "SELECT {t:" + TrackModel.PK + "} "
                + "FROM {" + TrackModel._TYPECODE +  " AS t}  WHERE {t:" + TrackModel.CODE + "}=" + code;

        final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString);

        return flexibleSearchService.<TrackModel>search(query).getResult();
    }
}
