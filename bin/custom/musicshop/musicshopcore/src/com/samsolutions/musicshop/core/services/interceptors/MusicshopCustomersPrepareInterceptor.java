package com.samsolutions.musicshop.core.services.interceptors;

import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import de.hybris.platform.servicelayer.user.daos.UserGroupDao;
import org.springframework.beans.factory.annotation.Required;

import java.util.HashSet;
import java.util.Set;

public class MusicshopCustomersPrepareInterceptor implements PrepareInterceptor<CustomerModel> {

    private static final String HYBRIDS = "hybrids";
    private UserGroupDao userGroupDao;

    @Override
    public void onPrepare(CustomerModel customerModel, InterceptorContext interceptorContext) throws InterceptorException {

        final UserGroupModel hybrids = userGroupDao.findUserGroupByUid(HYBRIDS);

        if (customerModel.getContactEmail().contains("test.com")) {
            if (!customerModel.getGroups().contains(hybrids)) {
                final Set<PrincipalGroupModel> newGroups = new HashSet<>(customerModel.getGroups());
                newGroups.add(hybrids);
                customerModel.setGroups(newGroups);
            }
        } else {
            if (customerModel.getGroups().contains(hybrids)) {
                final Set<PrincipalGroupModel> newGroups = new HashSet<PrincipalGroupModel>(customerModel.getGroups());
                newGroups.remove(hybrids);
                customerModel.setGroups(newGroups);
            }
        }

    }

    @Required
    public void setUserGroupDao(UserGroupDao userGroupDao) {
        this.userGroupDao = userGroupDao;
    }
}
