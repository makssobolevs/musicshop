package com.samsolutions.musicshop.core.services.impl;

import com.samsolutions.musicshop.core.model.AlbumModel;
import com.samsolutions.musicshop.core.model.TrackModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.url.impl.DefaultProductModelUrlResolver;
import de.hybris.platform.core.model.product.ProductModel;

public class MusicshopProductModelUrlResolver extends DefaultProductModelUrlResolver {

    private String trackPattern;

    private String albumPattern;

    public void setTrackPattern(String trackPattern) {
        this.trackPattern = trackPattern;
    }

    public void setAlbumPattern(String albumPattern) {
        this.albumPattern = albumPattern;
    }

    protected String getPattern(ProductModel productModel) {

        if (productModel.getItemtype().equals(TrackModel._TYPECODE)) {
            return trackPattern;
        }
        else if (productModel.getItemtype().equals(AlbumModel._TYPECODE)) {
            return albumPattern;
        }

        return getPattern();
    }

    @Override
    protected String resolveInternal(final ProductModel source)
    {
        final ProductModel baseProduct = getBaseProduct(source);

        final BaseSiteModel currentBaseSite = getBaseSiteService().getCurrentBaseSite();

        String url = getPattern(source);

        if (currentBaseSite != null && url.contains("{baseSite-uid}"))
        {
            url = url.replace("{baseSite-uid}", currentBaseSite.getUid());
        }
        if (url.contains("{category-path}"))
        {
            url = url.replace("{category-path}", buildPathString(getCategoryPath(baseProduct)));
        }
        if (url.contains("{product-name}"))
        {
            url = url.replace("{product-name}", urlSafe(baseProduct.getName()));
        }
        if (url.contains("{product-code}"))
        {
            url = url.replace("{product-code}", source.getCode());
        }

        return url;
    }


    @Override
    public String resolve(final ProductModel source)
    {
        final String key = getKey(source);
        if (key == null)
        {
            // No key, just return the value
            return resolveInternal(source);
        }

        // Lookup the cached value by key
        final String cachedValue = getThreadContextService().getAttribute(key);
        if (cachedValue != null)
        {
            return cachedValue;
        }

        final String url = resolveInternal(source);

        // Store the value in the thread cache
        getThreadContextService().setAttribute(key, url);

        return url;
    }
}
