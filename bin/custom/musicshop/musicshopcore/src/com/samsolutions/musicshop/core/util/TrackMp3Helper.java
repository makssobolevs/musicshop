package com.samsolutions.musicshop.core.util;

import com.mpatric.mp3agic.Mp3File;
import com.samsolutions.musicshop.core.jalo.Track;
import de.hybris.platform.jalo.media.Media;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;


public class TrackMp3Helper {

    private static final Logger log = LoggerFactory.getLogger(TrackMp3Helper.class);

    public Integer getDurationInSeconds(Track track) {
        Media mediaModel = track.getContent();
        Integer durationInSeconds = 0;

        if (mediaModel != null) {
            File file = mediaModel.getFile();
            try {
                Mp3File mp3File = new Mp3File(file);
                durationInSeconds = (int) mp3File.getLengthInSeconds();
            } catch (Exception e) {
                log.warn("Exception while loading track for TrackModel: {}", track.getPK());
            }
        }

        return durationInSeconds;

    }

}
