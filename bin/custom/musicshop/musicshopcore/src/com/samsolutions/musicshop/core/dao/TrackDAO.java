package com.samsolutions.musicshop.core.dao;

import com.samsolutions.musicshop.core.model.TrackModel;

import java.util.List;

public interface TrackDAO {

     List<TrackModel> findTracksByCode(String code);

}
