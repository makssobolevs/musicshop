package com.samsolutions.musicshop.core.services;

import com.samsolutions.musicshop.core.model.TrackModel;

import java.util.Optional;

public interface TrackService {

    Optional<TrackModel> getTrackForCode(final String code);

    String getDemoTrackUrlForTrack(final TrackModel trackModel);

}
